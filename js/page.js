$(document).ready(function () {
    $('#switcher .control-block').click(function () {
        if ($(this).hasClass('active-btn')) {
            return;
        }

        var item = $(this).data('item'),
            $current_item = $('.switch-content .switch-item.active-item'),
            $switched_item = $('.switch-content .switch-item[data-sw-item="' + item + '"]');

        $current_item.removeClass('active-item');

        setTimeout(function() {
            $switched_item.addClass('active-item');
        }, 500);

        $('#switcher .control-block.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
    });
});
